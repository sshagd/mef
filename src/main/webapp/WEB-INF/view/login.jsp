<%--
    Document   : index
    Created on : 25.10.2018, 10:22:51
    Author     : Patskevich.S
--%>
<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Главная</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <link href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css" rel='stylesheet' type="text/css">
        <link href="${pageContext.request.contextPath}/resources/css/dashboard.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet" type="text/css"/>

        <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.min.js"></script>

    </head>
    <body>
        <div class="container" style="width: 300px;">
    <c:url value="/perform_login" var="loginUrl" />
    <form action="${loginUrl}" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" class="form-control" name="username" placeholder="Email address" required autofocus value="login1">
        <input type="password" class="form-control" name="password" placeholder="Password" value="12345">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Вход</button>
    </form>
</div>

    </body>
</html>
