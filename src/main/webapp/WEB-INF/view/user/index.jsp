<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:index title="Пользователи">
    <jsp:attribute name="head">
        <script src="${pageContext.request.contextPath}/resources/js/users.js"></script>
    </jsp:attribute>

    <jsp:attribute name="filter">
        <form:form id="searchForm" modelAttribute="search">
            <div class="border border-light">
                <form:hidden path="page" cssClass="page j-page" value="1"/>
            </div>
            <div class="container" style="width: 100%">

            <div class="form-row">
                <div class="col">
                    <form:input path="sN" id="search" class="j-search" cssClass="form-control" placeholder="Фамилия"/>
                </div>
                <div class="col">
                    <form:input path="fN" id="search" class="j-search" cssClass="form-control" placeholder="Имя"/>
                </div>
                <div class="col">
                    <form:input path="lN" id="search" class="j-search" cssClass="form-control" placeholder="Отчество"/>
                </div>
            </div>
            <br>
            <div class="form-row">
                <div class="col">
                    <form:input path="login" id="search" class="j-search" cssClass="form-control" placeholder="Login"/>
                </div>
                <div class="col">
                    <form:input path="email" id="search" class="j-search" cssClass="form-control" placeholder="Email"/>
                </div>
                <div class="col">
                            <label for="blocked" class="col-form-label font-weight-bold">Все</label>
                            <form:radiobutton path="blocked" id="" value=""/>
                            <label for="blocked" class="col-form-label font-weight-bold">Blocked</label>
                            <form:radiobutton path="blocked" id="" value="true"/>
                            <label for="blocked" class="col-form-label font-weight-bold">Not blocked</label>
                            <form:radiobutton path="blocked" id="" value="false"/>
                </div>
            </div>

                <button type="button" class="btn btn-outline-primary j-search my-2">Поиск</button>
                <br>
                <button type="button" class="btn btn-primary j-create mb-4">Добавить пользователя</button>
            </div>
        </form:form>
    </jsp:attribute>
        
    <jsp:attribute name="body">

        <div class="j-table">
            <table class="table table-striped table-hover table-sm">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Фамилия</th>
                        <th scope="col">Имя</th>
                        <th scope="col">Отчество</th>
                        <th scope="col">login</th>
                        <th scope="col">email</th>
                        <th scope="col">blocked</th>
                        <th></th>
                        <th></th>
<%--                        <th></th>--%>
                    </tr>
                </thead>
                <tbody>
                    <c:set var="count" value="0"/>
                    <c:forEach var="item" items="${list.users}">
                        <tr>
                            <th scope="row"><c:set var="count" value="${count + 1}"/>${count}</th>
                            <td>${item.sN}</td>
                            <td>${item.fN}</td>
                            <td>${item.lN}</td>
                            <td>${item.login}</td>
                            <td>${item.email}</td>
                            <td>${item.blocked}</td>
                            <td><button type="button" class="btn btn-primary j-update" data-id="${item.uId}" data-toggle="tooltip" title="Редактировать"><i class="glyphicon glyphicon-edit"></i></button></td>
                            <td><button type="button" class="btn btn-info j-view" data-id="${item.uId}" data-toggle="tooltip" title="Просмотр"><i class="glyphicon glyphicon-eye-open"></i></button></td>
<%--                            <td><button type="button" class="btn btn-danger j-delete" data-id="${item.uId}" data-toggle="tooltip" title="Удалить" data-toggle="modal" data-target="#confirm-delete"><i class="glyphicon glyphicon-remove"></i></button></td>--%>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <div>
                <strong>Всего: </strong> ${list.total}
            </div>
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link text-dark" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link text-dark" id="pageNumb" data-page="1">1</a></li>
                    <li class="page-item">
                        <a class="page-link text-dark j-next" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </jsp:attribute>    
</t:index>
