<%--
  Created by IntelliJ IDEA.
  User: dev
  Date: 23.07.2019
  Time: 15:23
  To change this template use File | Settings | File Templates.
--%>
<<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="modal fade" id="${modalId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" id="result">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    ${title}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            <table class="table table-striped table-sm my-1">
                                <thead>
                                <tr>
                                    <th scope="col">Общие сведения:</th>
                                    <td scope="col"></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th>Имя</th>
                                    <td>${user.fN}</td>
                                </tr>
                                <tr>
                                    <th>Фамилия</th>
                                    <td>${user.sN}</td>
                                </tr>
                                <tr>
                                    <th>Отчество</th>
                                    <td>${user.lN}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm">
                            <table class="table table-striped table-sm my-1">
                                <thead>
                                <tr>
                                    <th scope="col">Учетные сведения:</th>
                                    <td scope="col"></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th>Логин</th>
                                    <td>${user.login}</td>
                                </tr>
                                <tr>
                                    <th>Заметки</th>
                                    <td>${user.notes}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>${user.email}</td>
                                </tr>
                                <tr>
                                    <th>Blocked</th>
                                    <td>${user.blocked}</td>
                                </tr>
                                <form:form id="user" modelAttribute="user">
                                    <c:forEach var="item" items="${status}">
                                        <tr>
                                            <th>${item.appName}</th>
                                            <td>
                                                <c:forEach var="r" items="${item.appRoles}">
                                                    <label>${r.roleName}</label>
                                                    <form:checkbox path="maps[${item.appName}][${r.roleName}]" value="" disabled="true"/>
                                                </c:forEach>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </form:form>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

