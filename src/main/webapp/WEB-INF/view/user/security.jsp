<%--
  Created by IntelliJ IDEA.
  User: Pagoda.A
  Date: 26.02.2020
  Time: 15:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="modal fade" id="${modalId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" id="result">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    ${title}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form:form id="user" modelAttribute="user">
                    <form:hidden path="uId" cssClass="id" />
                    <div class="border-bottom">
                        <label class="col-form-label font-weight-bold">Роли:</label>
                        <br>
                        <c:forEach var="item" items="${status}">
                            <label class="col-form-label font-weight-bold">${item.appName}</label>
                            <br>
                            <div class="j-table">
                                <table class="table table-hover table-sm">
                                    <thead>
                                        <tr>
                                            <th scope="col">Название</th>
                                            <th scope="col">Описание</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="r" items="${item.appRoles}">
                                            <tr>
                                                <td><label>${r.roleName}</label></td>
                                                <td><label>${r.description}</label></td>
                                                <td>
                                                    <form:checkbox path="maps[${item.appName}][${r.roleName}]" value=""/>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </c:forEach>
                    </div>
                </form:form>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success j-security" >Сохранить</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
