<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="modal fade" id="viewPerson" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" id="result">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    ${person.profile.FIO}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="user" modelAttribute="users">
                    <input type="hidden" class="type" name="type" value="${type}">
                    <input type="hidden" class="oid" name="oid" value="${person.oid}">
                    <div class="row border-bottom">        
                        <div class="col-4 border-right">
                            <div class="block">
                                <div class="block-title">
                                    <h5>Полномочия:</h5>
                                </div>			
                                <table class="table table-borderless table-striped table-vcenter table-sm">
                                    <tbody>
                                        <tr><th>Администратор</th><td><c:if test = "${not empty person.wapnee.role['ADMIN']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                        <tr><th>Отправка файлов</th><td><c:if test = "${not empty person.wapnee.role['SENDER']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                        <tr><th>Отчёты</th><td><c:if test = "${not empty person.wapnee.role['REPORTS']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                        <tr><th>Меры содействия </th><td><c:if test = "${not empty person.wapnee.role['ACTIONS']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                        <tr><th>Протоколы</th><td><c:if test = "${not empty person.wapnee.role['PROTOCOLS']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                        <tr><th>Уведомления</th><td><c:if test = "${not empty person.wapnee.role['INFORM']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                    </tbody>
                                </table>			                    
                            </div>
                            <div class="block" <c:if test = "${empty person.wapnee.role['SENDER']}">style="display: none;"</c:if>>
                                <div class="block-title">
                                    <h5>Directions присылаемых файлов:</h5>
                                </div>			
                                <table class="table table-borderless table-striped table-vcenter table-sm">
                                    <tbody>
                                        <tr><th>Исключение из списка</th><td><c:if test = "${not empty person.wapnee.directions['EXCLUDE']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                        <tr><th>Включение в список</th><td><c:if test = "${not empty person.wapnee.directions['INCLUDE']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                        <tr><th>Ребенок до 7 лет</th><td><c:if test = "${not empty person.wapnee.directions['BRINGUP']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                        <tr><th>Три и более ребенка</th><td><c:if test = "${not empty person.wapnee.directions['BRINGUP3']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                        <tr><th>Плательщик ЖКХ</th><td><c:if test = "${not empty person.wapnee.directions['PAYERCOMMUNAL']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                        <tr><th>Земельный участок</th><td><c:if test = "${not empty person.wapnee.directions['LANDOWNER']}"> <i class="fa fa-check"></i></c:if></td></tr>
                                    </tbody>
                                </table>
	
                                <table class="table table-borderless table-striped table-vcenter table-sm">
                                    <tbody>
                                        <tr>
                                            <th>Период:</th>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${person.wapnee.numPeriods == 1}">
                                                        Квартал
                                                    </c:when>
                                                    <c:when test="${person.wapnee.numPeriods == 2}">
                                                        Полугодие
                                                    </c:when>
                                                    <c:otherwise>                                                        
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-8 border-right">
                            <div class="block">
                                <div class="block-title">
                                    <h5>Общие сведения:</h5>
                                </div>			
                                <table class="table table-borderless table-striped table-vcenter table-sm">
                                    <tbody>
                                        <tr><th>УНП</th><td>${person.profile.UNP}</td></tr>
                                        <tr><th>Идентификационный номер</th><td>${person.profile.identif}</td></tr>
                                        <tr><th>ФИО</th><td>${person.profile.FIO}</td></tr>
                                        <tr><th>Организация</th><td>${person.profile.organization}</td></tr>
                                        <tr><th>Роль в комиссии</th><td>${person.profile.comRole}</td></tr>
                                        <tr><th>Наименование структурного подразделения</th><td>${person.profile.structSubdivision}</td></tr>
                                        <tr><th>Должность</th><td>${person.profile.position}</td></tr>
                                        <tr><th>Номер телефона</th><td>${person.profile.telNum}</td></tr>
                                        <tr><th>Электронная почта</th><td>${person.profile.email}</td></tr>
                                        
                                    </tbody>
                                </table>			                    
                            </div>
                        </div>
                    </div>
                </form>               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>                    
        </div>
    </div>
</div>
