<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="modal fade" id="${modalId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content" id="result">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    ${title}                    
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form:form id="user" modelAttribute="user">
                    <form:hidden path="uId" cssClass="id" />
                    <div class="row border-bottom">    
                        <div class="border-right col-6">
                            <label class="col-form-label font-weight-bold">Общие сведения:</label>
                            <div class="form-group">
                                <label for="fN" class="col-form-label font-weight-bold">Имя</label>
                                <form:input path="fN" cssClass="form-control" id="fN"/>
                            </div>
                            <div class="form-group">
                                <label for="lN" class="col-form-label font-weight-bold">Фамилия</label>
                                <form:input path="sN" cssClass="form-control" id="sN"/>
                            </div>
                            <div class="form-group">
                                <label for="lN" class="col-form-label font-weight-bold">Отчество</label>
                                <form:input path="lN" cssClass="form-control" id="lN"/>
                            </div>
                        </div>
                        <div class="border-right col-6">
                            <label class="col-form-label font-weight-bold">Учетные сведения:</label>
                            <div class="form-group">
                                <label for="login" class="col-form-label font-weight-bold">Логин</label>
                                <form:input path="login" cssClass="form-control" id="login" disabled="${disabled}"/>
                                <form:errors path="login" cssClass="error" />
                            </div>
                            <div class="form-group">
                                <label for="secret" class="col-form-label font-weight-bold">Пароль</label>
                                <form:password path="secret" cssClass="form-control" id="secret"/>
                                <form:errors path="secret" cssClass="error" />
                            </div>
                            <div class="form-group">
                                <label for="notes" class="col-form-label font-weight-bold">Заметки</label>
                                <form:input path="notes" cssClass="form-control" id="notes"/>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-form-label font-weight-bold">Email</label>
                                <form:input path="email" cssClass="form-control" id="email"/>
                            </div>
                            <div class="form-group">
                                <label for="blocked" class="col-form-label font-weight-bold">Blocked</label>
                                <form:checkbox path="blocked" value=""/>
                            </div>

                        </div>
                    </div>
                </form:form>               
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success j-save" >Сохранить</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>                    
        </div>
    </div>
</div>
