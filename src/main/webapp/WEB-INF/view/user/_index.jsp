<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<table class="table table-striped table-hover table-sm">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Фамилия</th>
            <th scope="col">Имя</th>
            <th scope="col">Отчество</th>
            <th scope="col">login</th>
            <th scope="col">email</th>
            <th scope="col">blocked</th>
            <th></th>
            <th></th>
<%--            <th></th>--%>
        </tr>
    </thead>
    <tbody>
        <c:set var="count" value="${startNumb}"/>
        <c:forEach var="item" items="${list.users}">
            <tr>
                <th scope="row"><c:set var="count" value="${count + 1}"/>${count}</th>
                <td>${item.sN}</td>
                <td>${item.fN}</td>
                <td>${item.lN}</td>
                <td>${item.login}</td>
                <td>${item.email}</td>
                <td>${item.blocked}</td>
                <td><button type="button" class="btn btn-primary j-update" data-id="${item.uId}" data-toggle="tooltip" title="Редактировать"><i class="glyphicon glyphicon-edit"></i></button></td>
                <td><button type="button" class="btn btn-info j-view" data-id="${item.uId}" data-toggle="tooltip" title="Просмотр"><i class="glyphicon glyphicon-eye-open"></i></button></td>
<%--                <td><button type="button" class="btn btn-danger j-delete" data-id="${item.uId}" data-toggle="tooltip" title="Удалить" data-toggle="modal" data-target="#confirm-delete"><i class="glyphicon glyphicon-remove"></i></button></td>--%>
            </tr>
        </c:forEach>
    </tbody>
</table>
<div>
    <strong>Всего: </strong> ${list.total}
</div>
<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center">
        <li class="page-item">
            <a class="page-link text-dark j-prev" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Предыдущая</span>
            </a>
        </li>
        <li class="page-item"><a class="page-link text-dark" id="pageNumb" data-page="${page}">${page}</a></li>
        <li class="page-item">
            <a class="page-link text-dark <c:if test="${list.c == 10}">j-next</c:if>" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Следующая</span>
            </a>
        </li>
    </ul>
</nav>
