<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="modal fade" uId="${modalId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" uId="result">
            <div class="modal-header">
                <h5 class="modal-title" uId="exampleModalLongTitle">
                    ${title}                    
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form:form uId="org" modelAttribute="org">
                    <form:hidden path="uId" cssClass="uId" />
                    <div class="form-group">
                       <label for="companyName" class="col-form-label font-weight-bold">Наименование организации</label>
                       <form:input path="companyName" cssClass="form-control" uId="companyName"/>
                     </div>
                    <div class="form-group">
                        <label for="UNP" class="col-form-label font-weight-bold">УНП</label>
                        <form:input path="UNP" cssClass="form-control" uId="companyName"/>
                    </div>
                    <div class="form-group">                        
                        <label for="districtCode" class="col-form-label font-weight-bold">Область</label>
                        <form:select path="districtCode" uId='districtCode' cssClass="form-control" items="${directionList}"/>
                    </div>
                    <div class="form-group">
                       <label for="м" class="col-form-label font-weight-bold">Район</label>
                       <form:input path="district" cssClass="form-control" uId="district"/>
                    </div>
                    <div class="form-group">
                       <label for="city" class="col-form-label font-weight-bold">Город</label>
                       <form:input path="city" cssClass="form-control" uId="city"/>
                    </div>
                </form:form>               
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-success j-save" >Сохранить</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>                    
        </div>
    </div>
</div>
