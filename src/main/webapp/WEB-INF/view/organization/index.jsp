<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:index title="Организации">
    <jsp:attribute name="head">
        <script src="${pageContext.request.contextPath}/resources/js/organization.js"></script>
    </jsp:attribute>
        
     <jsp:attribute name="filter">
         <form:form uId="searchForm" modelAttribute="search">
            <div class="border border-light p-3">
                <form:hidden path="page" cssClass="page j-page" value="1"/>
            </div>
        </form:form>
        <button type="button" class="btn btn-primary j-create mb-4">Добавить организацию</button>  
    </jsp:attribute>
        
    <jsp:attribute name="body">
        <div class="modal fade" uId="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        Удалить организацию
                    </div>
                    <div class="modal-body">
                        Вы действительно хотите удалить организацию из списка?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-success btn-ok" data-uId>Да</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="j-table">
            <table class="table table-striped table-hover table-sm">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Наименование</th>
                        <th scope="col">УНП</th>
                        <th scope="col">Область</th>
                        <th scope="col">Район</th>
                        <th scope="col">Город</th>  
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:set var="count" value="0"/>
                    <c:forEach var="item" items="${list.documents}">
                        
                        <tr>
                            <th scope="row"><c:set var="count" value="${count + 1}"/>${count}</th>
                            <td>${item.companyName}</td>
                            <td>${item.UNP}</td>
                            <td>${item.districtCodeName}</td>
                            <td>${item.district}</td>
                            <td>${item.city}</td>
                            <td><button type="button" class="btn btn-primary j-update" data-uId="${item.uId}" data-toggle="tooltip" title="Редактировать"><i class="glyphicon glyphicon-edit"></i></button></td>
                <td><button type="button" class="btn btn-info j-view" data-uId="${item.uId}" data-toggle="tooltip" title="Просмотр"><i class="glyphicon glyphicon-eye-open"></i></button></td>
                <td><button type="button" class="btn btn-danger j-delete" data-uId="${item.uId}" data-toggle="tooltip" title="Удалить" data-toggle="modal" data-target="#confirm-delete"><i class="glyphicon glyphicon-remove"></i></button></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <div>
                <strong>Всего: </strong> ${list.count}
            </div>
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                    <li class="page-item">
                        <a class="page-link text-dark" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link text-dark" uId="pageNumb" data-page="1">1</a></li>
                    <li class="page-item">
                        <a class="page-link text-dark <c:if test="${list.c == 10}">j-next</c:if>" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </jsp:attribute>    
</t:index>

                    
