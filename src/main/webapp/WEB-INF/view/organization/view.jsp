<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:index title="${org.companyName}" organization="active" role="${role}" login="${login}">
    <jsp:attribute name="head">
        <script src="${pageContext.request.contextPath}/resources/js/users.js"></script>
    </jsp:attribute>
     
    <jsp:attribute name="body">
        <div class="row border-bottom">        
            <div class="block">
                <table class="table table-borderless table-striped table-vcenter table-sm">
                    <tbody>
                        <tr><th>Наименование</th><td>${org.companyName}</td></tr>
                        <tr><th>УНП</th><td>${org.UNP}</td></tr>
                        <tr><th>Область</th><td>${org.districtCodeName}</td></tr>
                        <tr><th>Район</th><td>${org.district}</td></tr>
                        <tr><th>Город</th><td>${org.city}</td></tr>
                    </tbody>
                </table>			                    
            </div>  
        </div>
    </jsp:attribute> 
</t:index>