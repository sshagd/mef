<%@ page contentType="text/html" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<table class="table table-striped table-hover table-sm">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Наименование</th>
            <th scope="col">УНП</th>
            <th scope="col">Область</th>
            <th scope="col">Район</th>
            <th scope="col">Город</th>  
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <c:set var="count" value="0"/>
        <c:forEach var="item" items="${list.documents}">
            <tr>
                <th scope="row"><c:set var="count" value="${count + 1}"/>${count}</th>
                <td>${item.companyName}</td>
                <td>${item.UNP}</td>
                <td>${item.districtCodeName}</td>
                <td>${item.district}</td>
                <td>${item.city}</td>
                <td><button type="button" class="btn btn-primary j-update" data-uId="${item.uId}" data-toggle="tooltip" title="Редактировать"><i class="glyphicon glyphicon-edit"></i></button></td>
                <td><button type="button" class="btn btn-info j-view" data-uId="${item.uId}" data-toggle="tooltip" title="Просмотр"><i class="glyphicon glyphicon-eye-open"></i></button></td>
                <td><button type="button" class="btn btn-danger j-delete" data-uId="${item.uId}" data-toggle="tooltip" title="Удалить" data-toggle="modal" data-target="#confirm-delete"><i class="glyphicon glyphicon-remove"></i></button></td>
            </tr>
        </c:forEach>
    </tbody>
</table>
<div>
    <strong>Всего: </strong> ${list.count}
</div>
<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center">
        <li class="page-item">
            <a class="page-link text-dark j-prev" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Пердыдущая</span>
            </a>
        </li>
        <li class="page-item"><a class="page-link text-dark" uId="pageNumb" data-page="${page}">${page}</a></li>
        <li class="page-item">
            <a class="page-link text-dark <c:if test="${list.c == 10}">j-next</c:if>" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Следующая</span>
            </a>
        </li>
    </ul>
</nav>
