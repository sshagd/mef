﻿<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Simple Template" pageEncoding="UTF-8"%>
 
<%@attribute name="title"%>
<%@attribute name="head" fragment="true" %>
<%@attribute name="filter" fragment="true" %>
<%@attribute name="body" fragment="true" %>
 
<html>
    <head>
        <title>${title}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <link href="${pageContext.request.contextPath}/webjars/bootstrap/css/bootstrap.min.css" rel='stylesheet' type="text/css">
        <link href="${pageContext.request.contextPath}/webjars/font-awesome/css/all.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/webjars/bootstrap-glyphicons/css/bootstrap-glyphicons.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/webjars/Eonasdan-bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/resources/css/dashboard.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/resources/css/main.css" rel="stylesheet" type="text/css"/>

        <script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/webjars/momentjs/min/moment.min.js"></script>
        <script src="${pageContext.request.contextPath}/webjars/momentjs/min/moment-with-locales.min.js"></script>
        <script src="${pageContext.request.contextPath}/webjars/bootstrap/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/webjars/Eonasdan-bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
        <jsp:invoke fragment="head"/>
    </head>
    <body>
        <div>
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow fix-min-width top-menu">
                <a class="navbar-brand col-sm-3 col-md-2 mr-0 min-width180" href="#">Мониторинг условий труда</a>                
                <ul class="navbar-nav px-3 ml-auto">  
                    <li class="nav-item text-nowrap">
                        <a class="nav-link text-white" href="#">${login}</a>
                    </li>
                    <li class="nav-item text-nowrap">
                        <a class="nav-link" href="${pageContext.request.contextPath}/sign-in/logout">Sign out</a>
                    </li>
                </ul>
            </nav>
        </div>
        
        <div class="container-fluid">
            <div class="row">
                <nav class="col-md-2 d-none d-md-block bg-light sidebar min-width180">
                    <div class="sidebar-sticky">
                        <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link ${users}" href="${pageContext.request.contextPath}/user/index">
                                    <i class="fa fa-users"></i>
                                    Пользователи <span class="sr-only"></span>
                                </a>
                            </li>    
                            <li class="nav-item">
                                <a class="nav-link ${organization}" href="${pageContext.request.contextPath}/org/index">
                                    <i class="fa fa-users"></i>
                                    Организации <span class="sr-only"></span>
                                </a>
                            </li>
                        </ul>                       
                    </div>
                </nav> 
            </div>
        </div>
                                    
        <div class="j-modal"></div>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <jsp:invoke fragment="filter"/>
            <jsp:invoke fragment="body"/>
        </main>
    </body>
</html>
