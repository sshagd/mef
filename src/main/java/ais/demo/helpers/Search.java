package ais.demo.helpers;

import org.springframework.data.mongodb.core.query.Query;

/**
 *
 * @author Patskevich.S
 */
public interface Search {
	
	int getPage();
	int getaP();
	Query getQuery();
}