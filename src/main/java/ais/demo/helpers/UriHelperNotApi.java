/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.helpers;

import java.net.URI;

import ais.demo.models.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;
import ais.demo.models.helper.ConfigProp;

/**
 *
 * @author Patskevich.S
 */
@Service
public class UriHelperNotApi {
	
	@Autowired
	private ConfigProp config;
	
	public URI getLogin(String login){
		return UriComponentsBuilder.fromHttpUrl(config.getPrcaturl() + "/users" )
			.queryParam("aP", 100)
			.queryParam("sP", 0)
			.queryParam("fltr", "{\"login\":\""+login+"\"}")
			.build().encode().toUri();
	}
	
	public URI getFltrUrl(Search search, String collection){
		return UriComponentsBuilder.fromHttpUrl(config.getPrcaturl() + collection)
			.queryParam("aP", search.getaP())
			.queryParam("sP", (search.getPage() - 1) * search.getaP())
			.queryParam("fltr", search.getQuery().getQueryObject().toJson())
			.build()
			.encode()
			.toUri();
	}
	
	public URI getGETUri(String collection, String id){
		return UriComponentsBuilder.fromHttpUrl(config.getPrcaturl() + "/user/" + id)
			.build().encode().toUri();
	}
	
	public URI getPUTUri(String collection, String id){
		return UriComponentsBuilder.fromHttpUrl(config.getPrcaturl() + "/user/" + id)
			.build().encode().toUri();
	}
	
	public URI getPOSTUri(String collection){
		return UriComponentsBuilder.fromHttpUrl(config.getPrcaturl() + collection)
			.build().encode().toUri();
	}

	public URI keysUri(){
		return UriComponentsBuilder.fromHttpUrl(config.getKeysurl() + "/status")
			.build().encode().toUri();
	}

	public URI getKeyByUIdUri(String appName, String uId){
		return UriComponentsBuilder.fromHttpUrl(config.getKeysurl() + "/app/" + appName + "/user/" + uId)
				.build().encode().toUri();
	}

	public URI getKeyUri(String appName, String uId, String keyName){
		return UriComponentsBuilder
				.fromHttpUrl(config.getKeysurl() + "/app/" + appName + "/user/" + uId + "/key/" + keyName)
				.build().encode().toUri();
	}

}
