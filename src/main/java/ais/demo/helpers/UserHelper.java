package ais.demo.helpers;

import ais.demo.models.user.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import static org.springframework.util.ObjectUtils.isEmpty;

@Service
public class UserHelper {

    @Autowired
    private UserKeysImpl userKeysImpl;

    private List<Roles> newRole(){
        List<Roles> roles = new ArrayList<>();
        Roles role = new Roles();
        roles.add(role);
        List<String> r = new ArrayList<>();
        roles.get(0).setRoles(r);
        return roles;
    }

    public UserKey setRoleMap(UserKey key){
        if(isEmpty(key.getAppRoles())){
            key.setAppRoles(newRole());
        }
        if(isEmpty(key.getMaps())){
            Map<String, Map<String, Boolean>> roleMap = new HashMap<>();
            key.setMaps(roleMap);
        }
        for(int i = 0; i < key.getAppRoles().size(); i++){
            for (String str: key.getAppRoles().get(i).getRoles()) {
                key.getMaps().get(key.getAppRoles().get(i).getAppName()).put(str,
                        key.getAppRoles().get(i).getRoles().contains(str));
            }
        }
        return key;
    }

    public UserKey setRoleList(UserKey key){
        if(!isEmpty(key.getAppRoles())) {
            key.getAppRoles().clear();
            key.setAppRoles(newRole());
        }

        List<Roles> roles = new ArrayList<>();
        key.getMaps().entrySet().stream().forEach(item ->{
            Roles role = new Roles();
            role.setAppName(item.getKey());
            roles.add(role);
            key.setAppRoles(roles);
            for (Roles r: key.getAppRoles()) {
                List<String> list = new ArrayList<>();
                r.setRoles(list);
            }
        });

        for (Map.Entry<String, Map<String, Boolean>> map: key.getMaps().entrySet()) {
            map.getValue().entrySet().stream().forEach(item ->{
//                if(!isEmpty(item.getValue())){
//                    if(!item.getValue().equals(false)){
                        for(int i = 0; i < key.getMaps().size(); i++){
                            if(key.getAppRoles().get(i).getAppName().equals(map.getKey())) {
                                key.getAppRoles().get(i).getRoles().add(item.getKey());
                            }
                        }
//                    }
//                }
            });
        }
        return key;
    }

    private UserHelper userHelper;

    public User roles(User user, UserKeysList status){
        userHelper = new UserHelper();
        UserKey key = new UserKey();
        Map<String, Map<String, Boolean>> maps = new TreeMap<>();
        List<Roles> roles = new ArrayList<>();
        for (SecurityConfig conf: status.getSecurityConfig()) {
            roles.addAll(userKeysImpl.getKeyByUId(conf.getAppName(), user.getuId()).getBody().getAppRoles());
        }
        key.setAppRoles(roles);
        for (SecurityConfig conf: status.getSecurityConfig()) {
            maps.put(conf.getAppName(), new TreeMap<>());
        }
        user.setMaps(maps);
        key.setMaps(maps);
        userHelper.setRoleMap(key);
        user.setMaps(key.getMaps());
        return user;
    }

    public User newRoles(User user, UserKeysList status){
        userHelper = new UserHelper();
        UserKey key = new UserKey();
        Map<String, Map<String, Boolean>> maps = new TreeMap<>();
        for (SecurityConfig conf: status.getSecurityConfig()) {
            maps.put(conf.getAppName(), new TreeMap<>());
        }
        key.setMaps(maps);
        List<Roles> roles1 = new ArrayList<>();
        for (SecurityConfig conf: status.getSecurityConfig()) {
        Roles roles = new Roles();
        List<String> roles11 = new ArrayList<>();
            roles.setRoles(roles11);
            roles.setAppName(conf.getAppName());
            roles1.add(roles);
        }
            key.setAppRoles(roles1);
        setRoleMap(key);
        user.setMaps(key.getMaps());

        return user;
    }

}
