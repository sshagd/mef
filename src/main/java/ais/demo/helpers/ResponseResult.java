/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.helpers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Patskevich.S
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseResult {
	
	public String result;
	@JsonProperty("_id")
	public String id;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	/*@SuppressWarnings("unchecked")
    @JsonProperty("_id")
    private void unpackOid(Map<String,Object> oid) {
        this.id = (String)oid.get("$oid");
    }*/

	@Override
	public String toString() {
		return "ResponseResult{" + "result=" + result + ", id=" + id + '}';
	}
	
	
}
