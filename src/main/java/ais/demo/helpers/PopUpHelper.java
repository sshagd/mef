/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

/**
 *
 * @author Patskevich.S
 */
@Service
public class PopUpHelper {
	
	final static List<Integer> CODE = new ArrayList<>();
	static{
		CODE.add(200);
		CODE.add(2001);
		CODE.add(2002);
		CODE.add(201);
		CODE.add(400);
		CODE.add(403);
		CODE.add(404);
		CODE.add(500);
		CODE.add(520);
	}
	
	final static Map<Integer, String> CSSSTYLEALERTCODE = new HashMap<>();
	static{
		CSSSTYLEALERTCODE.put(200, "alert-success");
		CSSSTYLEALERTCODE.put(2001, "alert-success");
		CSSSTYLEALERTCODE.put(2002, "alert-success");
		CSSSTYLEALERTCODE.put(201, "alert-success");
		CSSSTYLEALERTCODE.put(400, "alert-warning");
		CSSSTYLEALERTCODE.put(403, "alert-warning");
		CSSSTYLEALERTCODE.put(404, "alert-warning");
		CSSSTYLEALERTCODE.put(500, "alert-danger");
		CSSSTYLEALERTCODE.put(520, "alert-danger");
	}
	
	final static Map<Integer, String> TEXTUSER = new HashMap<>();
	static{
		TEXTUSER.put(200, "Пользователь добавлен");
		TEXTUSER.put(2001, "Пользователь обновлен");
		TEXTUSER.put(2002, "Пользователь удален");
		TEXTUSER.put(201, "");
		TEXTUSER.put(400, "Невозможно выполнить это действие");
		TEXTUSER.put(403, "Недостаточно прав для выполнения данного действия");
		TEXTUSER.put(404, "Запись не найдена");
		TEXTUSER.put(500, "Произошла ошибка при выполнении этого действия");
		TEXTUSER.put(520, "Произошла неизвестная ошибка");
	}
	
	final static Map<Integer, String> TEXTORG = new HashMap<>();
	static{
		TEXTORG.put(200, "Организация обновлена");
		TEXTORG.put(2002, "Организация удалена");
		TEXTORG.put(201, "Организация добавлена");
		TEXTORG.put(400, "Невозможно выполнить это действие");
		TEXTORG.put(403, "Недостаточно прав для выполнения данного действия");
		TEXTORG.put(404, "Запись не найдена");
		TEXTORG.put(500, "Произошла ошибка при выполнении этого действия");
		TEXTORG.put(520, "Произошла неизвестная ошибка");
	}
	
	final static Map<Integer, String> TEXTCERT = new HashMap<>();
	static{
		TEXTCERT.put(200, "Аттестация обновлена");
		TEXTCERT.put(2002, "Аттестация удалена");
		TEXTCERT.put(201, "Аттестация добавлена");
		TEXTCERT.put(400, "Невозможно выполнить это действие");
		TEXTCERT.put(403, "Недостаточно прав для выполнения данного действия");
		TEXTCERT.put(404, "Запись не найдена");
		TEXTCERT.put(500, "Произошла ошибка при выполнении этого действия");
		TEXTCERT.put(520, "Произошла неизвестная ошибка");
	}
	
	final static Map<Integer, String> TEXTCARD = new HashMap<>();
	static{
		TEXTCARD.put(200, "Карта аттестации обновлена");
		TEXTCARD.put(2002, "Карта аттестации удалена");
		TEXTCARD.put(201, "Карта аттестации добавлена");
		TEXTCARD.put(400, "Невозможно выполнить это действие");
		TEXTCARD.put(403, "Недостаточно прав для выполнения данного действия");
		TEXTCARD.put(404, "Запись не найдена");
		TEXTCARD.put(500, "Произошла ошибка при выполнении этого действия");
		TEXTCARD.put(520, "Произошла неизвестная ошибка");
	}
	
	public Model popUpModel(Model model, Integer code, String modalSize, String modelName, String method){

		code = adaptCode(method, code, modelName);

		if(CODE.contains(code)){
			switch (modelName) {
				case "User":
					model.addAttribute("text", TEXTUSER.get(code));
					break;
				case "Organization":
					model.addAttribute("text", TEXTORG.get(code));
					break;
				case "Certification":
					model.addAttribute("text", TEXTCERT.get(code));
					break;
				case "Card":
					model.addAttribute("text", TEXTCARD.get(code));
					break;
				default:
					break;
			}
			model.addAttribute("cssStyle", CSSSTYLEALERTCODE.get(code));
			model.addAttribute("modalSize", modalSize);
		} else {
			model.addAttribute("text", TEXTUSER.get(520));
			model.addAttribute("cssStyle", CSSSTYLEALERTCODE.get(520));
			model.addAttribute("modalSize", "");
		}
		return model;
	}
	
	private Integer adaptCode(String method, Integer code, String modelName){
		
		if(modelName.equals("User")){
			if(code == 200 && method.equals("PUT"))
			{
				code = 2001;
			}

			if(code == 200 && method.equals("DELETE"))
			{
				code = 2002;
			}			
		}
		
		if(modelName.equals("Organization")){
			if(code == 200 && method.equals("DELETE"))
			{
				code = 2002;
			}			
		}
		
		if(modelName.equals("Certification")){
			if(code == 200 && method.equals("DELETE"))
			{
				code = 2002;
			}			
		}
		
		if(modelName.equals("Card")){
			if(code == 200 && method.equals("DELETE"))
			{
				code = 2002;
			}			
		}
		
		System.out.println(code);
		
		return code;
	}
}
