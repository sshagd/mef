/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.helper;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

/**
 *
 * @author Patskevich.S
 */
public class ObjId implements Serializable{
	
	@JsonProperty("$oid")
    private String oid;
	
	public ObjId (){
		this.oid = "";
	}
	
	public ObjId (String oid){
		this.oid = oid;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}
}
