/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.helper;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 *
 * @author Patskevich.S
 */
@Configuration
@PropertySource("classpath:configprops.properties")
@ConfigurationProperties(prefix="my")
public class ConfigProp {
	
	private String uploadfolder;
	private String notapiurl;
	private String apiurl;
	private String prcaturl;
	private String keysurl;

	public String getUploadfolder() {
		return uploadfolder;
	}

	public void setUploadfolder(String uploadfolder) {
		this.uploadfolder = uploadfolder;
	}

	public String getNotapiurl() {
		return notapiurl;
	}

	public void setNotapiurl(String notapiurl) {
		this.notapiurl = notapiurl;
	}

	public String getApiurl() {
		return apiurl;
	}

	public void setApiurl(String apiurl) {
		this.apiurl = apiurl;
	}

	public String getPrcaturl() {
		return prcaturl;
	}

	public void setPrcaturl(String prcaturl) {
		this.prcaturl = prcaturl;
	}

	public String getKeysurl() {
		return keysurl;
	}

	public void setKeysurl(String keysurl) {
		this.keysurl = keysurl;
	}
}
