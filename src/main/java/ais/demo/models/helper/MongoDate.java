/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.helper;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Patskevich.S
 */
public class MongoDate {
	
	@JsonProperty("$date")
    private Long date;
	
	public MongoDate (Long date){
		this.date = date;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}
	
}
