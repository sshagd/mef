/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.organization;

import ais.demo.helpers.Search;
import org.springframework.data.mongodb.core.query.Query;

/**
 *
 * @author Patskevich.S
 */
public class OrganizationListSearch implements Search{
	private int page;
	private int aP = 10;

	private Query query;
	
	@Override
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	@Override
	public int getaP() {
		return aP;
	}

	public void setaP(int aP) {
		this.aP = aP;
	}	

	@Override
	public Query getQuery() {
		
		query = new Query();		
		return query;
	}
}

