/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.organization;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Patskevich.S
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrganizationList {
	
	private final List<Organization> documents;
	private String companyName;
	private int count;
	private int c;
	
	public Map getOrgList(){
		Map<String, String> list = new HashMap <>();
		list.put("", "");
		documents.forEach((org) -> {
			list.put(org.getId(), org.getCompanyName());
		});
		
		return list;
	}
	
	public OrganizationList() {
        documents = new ArrayList<>();
    }

	public List<Organization> getDocuments() {
		return documents;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getCount() {
		return count;
	}
	
	public int getC() {
		return documents.size();
	}
	
	@Override
    public String toString() {
        return "Organizations{" +
                "documents=" + documents +
				"count=" + count +
				", c=" + c +
                '}';
    }
}
