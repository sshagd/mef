/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.organization;

import ais.demo.helpers.ResponseResult;
import ais.demo.helpers.UriHelperNotApi;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Patskevich.S
 */
@Service
public class OrganizationImpl {
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	UriHelperNotApi uriHelperN;

	private HttpMethod hm;
	private URI uri;
	private final String COLLECTION = "org";
	
	private HttpEntity getEntity(Object obj) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		HttpHeaders HEADERS = new HttpHeaders();	
		HEADERS.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<>(mapper.writeValueAsString(obj),HEADERS);
	}
	
	public ResponseEntity<OrganizationList> getList(OrganizationListSearch search){
		ResponseEntity<OrganizationList> response = restTemplate.getForEntity(uriHelperN.getFltrUrl(search, COLLECTION), OrganizationList.class);

		return response;
	}
	
	public ResponseEntity<Organization> getOne(String orgId){
		ResponseEntity<Organization> response = restTemplate.getForEntity(uriHelperN.getGETUri(COLLECTION, orgId), Organization.class);

		return response;
	}
	
	public ResponseEntity<ResponseResult> post(Organization org){
		ResponseEntity<ResponseResult> response;
		try{
			restTemplate.exchange(uriHelperN.getPOSTUri(COLLECTION), HttpMethod.POST, getEntity(org), String.class);
			response = new ResponseEntity<>(HttpStatus.CREATED);
		} catch(Exception e) {
			response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return response;
	}
	
	public ResponseEntity<ResponseResult> put(Organization org){
		ResponseEntity<ResponseResult> response;
		Organization orgOld = this.getOne(org.getId()).getBody();
		if(!org.equals(orgOld)){
			try{
				restTemplate.exchange(uriHelperN.getPUTUri(COLLECTION, org.getId()), HttpMethod.PUT, getEntity(org), String.class);
				response = new ResponseEntity<>(HttpStatus.OK);
			} catch(Exception e) {
				response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} else {
			response = new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
		}

		return response;
	}
	
	public HttpStatus delete(String orgId){
		hm = HttpMethod.DELETE;
		
		ResponseEntity<String> response = restTemplate.exchange(uriHelperN.getGETUri(COLLECTION, orgId), hm, null, String.class);
		return response.getStatusCode();
	}
}
