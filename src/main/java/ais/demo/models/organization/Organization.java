/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.organization;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Patskevich.S
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Organization {
	
	@JsonIgnore
	private String id;
	private String CompanyName;
	private String UNP;
	private int DistrictCode;
	@JsonIgnore
	private String DistrictCodeName;
	private String District;	
	private String City;

	@JsonIgnore
	public Map getDistrictCodeList()
	{
		Map<Integer, String> list = new HashMap <>();
		
		list.put(0, "");
		list.put(1, "г. Минск");
		list.put(2, "Минская");
		list.put(3, "Брестская");
		list.put(4, "Витебская");
		list.put(5, "Гомельская");
		list.put(6, "Гродненская");
		list.put(7, "Могилевская");
		
		return list;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	@JsonProperty("CompanyName")
	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String CompanyName) {
		this.CompanyName = CompanyName;
	}
	@JsonProperty("UNP")
	public String getUNP() {
		return UNP;
	}

	public void setUNP(String UNP) {
		this.UNP = UNP;
	}
	@JsonProperty("DistrictCode")
	public int getDistrictCode() {
		return DistrictCode;
	}

	public void setDistrictCode(int DistrictCode) {
		this.DistrictCode = DistrictCode;
	}

	public String getDistrictCodeName() {
		return DistrictCodeName;
	}
	@JsonProperty("District")
	public String getDistrict() {
		return District;
	}

	public void setDistrict(String District) {
		this.District = District;
	}
	@JsonProperty("City")
	public String getCity() {
		return City;
	}

	public void setCity(String City) {
		this.City = City;
	}
	
	@SuppressWarnings("unchecked")
    @JsonProperty("_id")
    private void unpackOid(Map<String,Object> id) {
        this.id = (String)id.get("$oid");
    }
	
	@SuppressWarnings("unchecked")
    @JsonProperty("DistrictCode")
    private void unpackDistrictCode(int DistrictCode) {
        this.DistrictCode = DistrictCode;
		this.DistrictCodeName = (String)getDistrictCodeList().get(DistrictCode);
    }
	
	@Override
    public String toString() {
        return "Organization{" +
                "DistrictCode=" + DistrictCode +
				", DistrictCodeName='" + DistrictCodeName + '\'' +
                ", id='" + id + '\'' +
                ", CompanyName='" + CompanyName + '\'' +
                ", UNP='" + UNP +  '\'' +
				", District='" + District + '\'' +
				", City='" + City + '\'' +
                '}';
    }
}
