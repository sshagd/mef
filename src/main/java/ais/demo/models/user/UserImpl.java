/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.user;

import ais.demo.helpers.ResponseResult;
import ais.demo.helpers.UriHelperNotApi;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Patskevich.S
 */
@Service
public class UserImpl {
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	UriHelperNotApi uriHelperN;

	private HttpMethod hm;
	private URI uri;
	private final String COLLECTION = "users";
	
	private HttpEntity getEntity(Object obj) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		HttpHeaders HEADERS = new HttpHeaders();	
		HEADERS.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<>(mapper.writeValueAsString(obj),HEADERS);
	}
	
	public ResponseEntity<UserList> getList(UserListSearch search){
		ResponseEntity<UserList> response = restTemplate.getForEntity(uriHelperN.getFltrUrl(search, COLLECTION), UserList.class);

		return response;
	}

	public ResponseEntity<User> getOne(String userId){
		ResponseEntity<User> response = restTemplate.getForEntity(uriHelperN.getGETUri(COLLECTION, userId), User.class);

		return response;
	}
	
	public User post(User user){
		ResponseEntity<ResponseResult> response;
		try{
			user = restTemplate.exchange(uriHelperN.getPOSTUri(COLLECTION), HttpMethod.POST, getEntity(user), User.class).getBody();
			response = new ResponseEntity<>(HttpStatus.CREATED);
		} catch(Exception e) {

			response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return user;
	}
	
	public ResponseEntity<ResponseResult> put(User user){
		ResponseEntity<ResponseResult> response;
		User userOld = this.getOne(user.getuId()).getBody();
		if(!user.equals(userOld)){
			try{
				restTemplate.exchange(uriHelperN.getPUTUri(COLLECTION, user.getuId()), HttpMethod.PUT, getEntity(user), String.class);
				response = new ResponseEntity<>(HttpStatus.OK);
			} catch(Exception e) {
				response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} else {
			response = new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
		}

		return response;
	}

//	public HttpStatus delete(String userId){
//		hm = HttpMethod.DELETE;
//
//		ResponseEntity<String> response = restTemplate.exchange(uriHelperN.getGETUri(COLLECTION, userId), hm, null, String.class);
//		System.out.println("response " + response);
//		return response.getStatusCode();
//	}
}
