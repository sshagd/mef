/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Patskevich.S
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable{

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String uId;
	private String fN = "";
	private String sN = "";
	private String lN = "";
	private String login;
	private String email;
	private String secret;
	private String notes;
	private boolean blocked;
	@JsonIgnore
	private Map<String, Map<String, Boolean>> maps;

	public User(){}

	public String getuId() {
		return uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	public String getfN() {
		return fN;
	}

	public void setfN(String fN) {
		this.fN = fN;
	}

	public String getsN() {
		return sN;
	}

	public void setsN(String sN) {
		this.sN = sN;
	}

	public String getlN() {
		return lN;
	}

	public void setlN(String lN) {
		this.lN = lN;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public Map<String, Map<String, Boolean>> getMaps() {
		return maps;
	}

	public void setMaps(Map<String, Map<String, Boolean>> maps) {
		this.maps = maps;
	}

	@Override
	public String toString() {
		return "User{" +
				"uId='" + uId + '\'' +
				", firstname=" + fN +
				", secondname=" + sN +
				", lastname=" + lN +
				", login='" + login + '\'' +
				", email=" + email +
				", secret='" + secret + '\'' +
				", notes='" + notes + '\'' +
				", blocked=" + blocked +
				'}';
	}
}
