package ais.demo.models.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import java.util.Map;

public class UserKey {

    private List<Roles> appRoles;
    private String uId;
    @JsonIgnore
    private Map<String, Map<String, Boolean>> maps;

    public List<Roles> getAppRoles() {
        return appRoles;
    }

    public void setAppRoles(List<Roles> appRoles) {
        this.appRoles = appRoles;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public Map<String, Map<String, Boolean>> getMaps() {
        return maps;
    }

    public void setMaps(Map<String, Map<String, Boolean>> maps) {
        this.maps = maps;
    }

    @Override
    public String toString() {
        return "UserKey{" +
                "appRoles=" + appRoles +
                ", uId='" + uId + '\'' +
                '}';
    }
}
