package ais.demo.models.user;

import java.util.List;

public class AppRoles {

    private String description;
    private String roleName;
    private List<String> securityGroups;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public List<String> getSecurityGroups() {
        return securityGroups;
    }

    public void setSecurityGroups(List<String> securityGroups) {
        this.securityGroups = securityGroups;
    }

    @Override
    public String toString() {
        return "AppRoles{" +
                "description='" + description + '\'' +
                ", roleName='" + roleName + '\'' +
                ", securityGroups=" + securityGroups +
                '}';
    }
}
