package ais.demo.models.user;

import java.util.List;

public class Roles {

    private String appName;
    private List<String> roles;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "Roles{" +
                "appName='" + appName + '\'' +
                ", roles=" + roles +
                '}';
    }
}
