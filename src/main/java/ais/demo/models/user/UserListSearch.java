/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.user;

import ais.demo.helpers.Search;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import static org.springframework.util.ObjectUtils.isEmpty;

/**
 *
 * @author Patskevich.S
 */
public class UserListSearch implements Search{

    private String fN;
    private String sN;
    private String lN;
    private String login;
    private String email;
    private String blocked;
	private int page;
	private int aP = 10;
	private Query query;

    @Override
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	@Override
	public int getaP() {
		return aP;
	}

	public void setaP(int aP) {
		this.aP = aP;
	}

    public String getfN() {
        return fN;
    }

    public void setfN(String fN) {
        this.fN = fN;
    }

    public String getsN() {
        return sN;
    }

    public void setsN(String sN) {
        this.sN = sN;
    }

    public String getlN() {
        return lN;
    }

    public void setlN(String lN) {
        this.lN = lN;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBlocked() {
        return blocked;
    }

    public void setBlocked(String blocked) {
        this.blocked = blocked;
    }

    @Override
	public Query getQuery() {

		query = new Query();
		Criteria criteria = new Criteria();

		if(!isEmpty(sN)){
			criteria.and("sN").is(sN);
		}

		if(!isEmpty(fN)){
			criteria.and("fN").is(fN);
		}

		if(!isEmpty(lN)){
			criteria.and("lN").is(lN);
		}

        if(!isEmpty(login)){
            criteria.and("login").is(login);
        }

        if(!isEmpty(email)){
            criteria.and("email").is(email);
        }

        if(!isEmpty(blocked)){
            if(blocked.equals("true")){
                criteria.and("blocked").is(true);
            }
            if(blocked.equals("false")){
                criteria.and("blocked").is(false);
            }
        }

		query.addCriteria(criteria);

		return query;
	}

    @Override
    public String toString() {
        return "Search{" +
                "firstname=" + fN +
                ", secondname=" + sN +
                ", lastname=" + lN +
                ", login='" + login + '\'' +
                ", email=" + email +
                ", blocked=" + blocked +
                '}';
    }
}
