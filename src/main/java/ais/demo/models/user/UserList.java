/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.models.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Patskevich.S
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserList implements Serializable{
	
	private final List<User> users;
	private int total;
	private int c;
	
	
	public UserList() {
        users = new ArrayList<>();
    }

	public List<User> getUsers() {
		return users;
	}

	public int getTotal() {
		return total;
	}

	public int getC() {
		return users.size();
	}
	
	@Override
    public String toString() {
        return "Users{" +
                "users=" + users +
				", total=" + total +
				", c=" + c +
                '}';
    }
}
