package ais.demo.models.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserKeysList implements Serializable {

    private final List<SecurityConfig> securityConfig;
//    private int total;
//    private int c;


    public UserKeysList() {
        securityConfig = new ArrayList<>();
    }

    public List<SecurityConfig> getSecurityConfig() {
        return securityConfig;
    }

    public int getC() {
        return securityConfig.size();
    }

    @Override
    public String toString() {
        return "UserKeysList{" +
                "securityConfig=" + securityConfig +
                '}';
    }
}
