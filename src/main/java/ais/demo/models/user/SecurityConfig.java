package ais.demo.models.user;

import java.util.List;

public class SecurityConfig {

    private String appName;
    private List<AppRoles> appRoles;
    private List<SecurityGroups> securityGroups;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public List<AppRoles> getAppRoles() {
        return appRoles;
    }

    public void setAppRoles(List<AppRoles> appRoles) {
        this.appRoles = appRoles;
    }

    public List<SecurityGroups> getSecurityGroups() {
        return securityGroups;
    }

    public void setSecurityGroups(List<SecurityGroups> securityGroups) {
        this.securityGroups = securityGroups;
    }

    @Override
    public String toString() {
        return "SecurityConfig{" +
                "appName='" + appName + '\'' +
                ", appRoles=" + appRoles +
                ", securityGroups=" + securityGroups +
                '}';
    }
}
