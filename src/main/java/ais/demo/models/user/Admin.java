package ais.demo.models.user;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Admin {
    private List<String> role = new ArrayList<>();
    @JsonIgnore
    private Map<String, Boolean> roleMap = new HashMap<>();

    public Admin() {}

    public List<String> getRole() {
        return role;
    }

    public void setRole(List<String> role) {
        this.role = role;
    }

    public Map<String, Boolean> getRoleMap() {
        return roleMap;
    }

    public void setRoleMap(Map<String, Boolean> roleMap) {
        this.roleMap = roleMap;
    }
}
