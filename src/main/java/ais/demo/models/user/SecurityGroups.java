package ais.demo.models.user;

import java.util.List;

public class SecurityGroups {

    private String groupName;
    private List<String> keys;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    @Override
    public String toString() {
        return "SecurityGroups{" +
                "groupName='" + groupName + '\'' +
                ", keys=" + keys +
                '}';
    }
}
