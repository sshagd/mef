package ais.demo.models.user;

import ais.demo.helpers.ResponseResult;
import ais.demo.helpers.UriHelperNotApi;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.net.URI;
import java.util.List;

@Service
public class UserKeysImpl {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    UriHelperNotApi uriHelperN;

    private HttpEntity getEntity(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        HttpHeaders HEADERS = new HttpHeaders();
        HEADERS.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(mapper.writeValueAsString(obj),HEADERS);
    }

    public ResponseEntity<UserKeysList> getList(){
        ResponseEntity<UserKeysList> response = restTemplate.getForEntity(uriHelperN.keysUri(), UserKeysList.class);
        return response;
    }

    public ResponseEntity<UserKey> getKeyByUId(String appName, String uId) {
        ResponseEntity<UserKey> response = restTemplate.getForEntity(uriHelperN.getKeyByUIdUri(appName, uId), UserKey.class);
        return response;
    }

    public ResponseEntity<String> getKey(String appName, String uId, String keyName){
        ResponseEntity<String> response = restTemplate
                .getForEntity(uriHelperN.getKeyUri(appName, uId, keyName), String.class);
        return response;
    }

    public ResponseEntity<ResponseResult> put(UserKeyPut key){
        ResponseEntity<ResponseResult> response;
            try{
                restTemplate.exchange(uriHelperN.getKeyByUIdUri(
                        key.getAppName(), key.getuId()), HttpMethod.PUT, getEntity(key), String.class);
                response = new ResponseEntity<>(HttpStatus.OK);
            } catch(Exception e) {
                response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        return response;
    }
}
