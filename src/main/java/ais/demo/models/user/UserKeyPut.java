package ais.demo.models.user;

import java.util.List;

public class UserKeyPut {

    private String appName;
    private List<String> roles;
    private String uId;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    @Override
    public String toString() {
        return "UserKeyPut{" +
                "appName='" + appName + '\'' +
                ", roles=" + roles +
                ", uId='" + uId + '\'' +
                '}';
    }
}
