///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package ais.demo.models.user;
//
//import java.io.Serializable;
//
///**
// *
// * @author Patskevich.S
// */
//public class Profile implements Serializable{
//
//	private String firstname = "";
//	private String lastname = "";
//	private String middlename = "";
//	private String phone = "";
//	private String position = "";
//
//	public String getfN() {
//		return firstname;
//	}
//
//	public void setfN(String firstname) {
//		this.firstname = firstname;
//	}
//
//	public String getlN() {
//		return lastname;
//	}
//
//	public void setlN(String lastname) {
//		this.lastname = lastname;
//	}
//
//	public String getMiddlename() {
//		return middlename;
//	}
//
//	public void setMiddlename(String middlename) {
//		this.middlename = middlename;
//	}
//
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	public String getPosition() {
//		return position;
//	}
//
//	public void setPosition(String position) {
//		this.position = position;
//	}
//
//
//	@Override
//    public String toString() {
//        return "Profile{" +
//                "firstname='" + firstname + '\'' +
//                ", lastname='" + lastname + '\'' +
//                ", middlename='" + middlename +  '\'' +
//				", phone='" + phone + '\'' +
//				", position='" + position + '\'' +
//                '}';
//    }
//
//	public String toJSON() {
//        return "{" +
//                "\"firstname\":\"" + firstname + '"' +
//                ", \"lastname\":\"" + lastname + '"' +
//                ", \"middlename\":\"" + middlename +  '"' +
//				", \"phone\":\"" + phone + '"' +
//				", \"position\":\"" + position + '"' +
//                '}';
//    }
//
//}
