/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.config;

import ais.demo.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


/**
 *
 * @author Patskevich.S
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
    UserDetailsServiceImpl userDetailsService;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	};

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.
			userDetailsService(userDetailsService).
			passwordEncoder(passwordEncoder());
	}

	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
				.authorizeRequests()
                //.antMatchers("/", "/user/index2", "/user/create","/user/add", "/webjars/**", "/resources/**").permitAll()
				.antMatchers("/webjars/**", "/resources/**").permitAll()
                .anyRequest().authenticated()
				//.anyRequest().hasRole("globalAdm")
				.and();
        http.formLogin()
                // указываем страницу с формой логина
                .loginPage("/sign-in/login")
                // указываем action с формы логина
                .loginProcessingUrl("/perform_login")
                // указываем URL при неудачном логине
                .failureUrl("/sign-in/login?error")
                // Указываем параметры логина и пароля с формы логина
                .usernameParameter("username")
                .passwordParameter("password")
                // даем доступ к форме логина всем
                .permitAll();
        http.logout()
                // разрешаем делать логаут всем
                .permitAll()
                // указываем URL логаута
                .logoutUrl("/sign-in/logout")
                // указываем URL при удачном логауте
                .logoutSuccessUrl("/sign-in/login?logout")
                // делаем не валидной текущую сессию
                .invalidateHttpSession(true);
    }
}
