/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.config;

import java.nio.charset.Charset;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;


/**
 *
 * @author Patskevich.S
 */
@Configuration
@ComponentScan("ais.demo")
public class AppConfig {
	
	@Bean
	public RestTemplate getRestTemplate() {
		RestTemplate RT = new RestTemplate();
		RT.getMessageConverters() 
			.add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
		return RT;
	}
}
