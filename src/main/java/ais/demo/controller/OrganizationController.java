/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.controller;

import ais.demo.helpers.PopUpHelper;
import ais.demo.models.organization.Organization;
import ais.demo.models.organization.OrganizationImpl;
import ais.demo.models.organization.OrganizationListSearch;
//import ais.demo.service.AuthenticationFacade;
import java.io.IOException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import static org.springframework.util.StringUtils.isEmpty;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Patskevich.S
 */
@Controller
@RequestMapping("/org")
public class OrganizationController {

	private int responseCode;
	private final String modalId = "orgModal";

//	@Autowired
//	private AuthenticationFacade auth;

	@Autowired
	private OrganizationImpl orgImpl;

	@Autowired
	private PopUpHelper popUp;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
    public String actionIndex(Model model) throws IOException{

		OrganizationListSearch search = new OrganizationListSearch();

		model.addAttribute("list", orgImpl.getList(search).getBody());
		model.addAttribute("search", search);
//		model.addAttribute("role", auth.getRole());
//		model.addAttribute("login", auth.getLogin());

        return "organization/index";
    }

	@RequestMapping(value = "/indexRefresh", method = RequestMethod.POST)
    public String actionIndexRefresh(
				@ModelAttribute("search")OrganizationListSearch search,
				Model model){

		model.addAttribute("list", orgImpl.getList(search).getBody());
		model.addAttribute("page", search.getPage());
        model.addAttribute("startNumb", (search.getPage() - 1)  * 10);

        return "organization/_index";
    }

	@RequestMapping(value = "/view", method = RequestMethod.GET)
    public String actionView(Model model) {

//		Organization org = orgImpl.getOne(auth.getOrgId()).getBody();

//        model.addAttribute("org", org);
//		model.addAttribute("role", auth.getRole());
//		model.addAttribute("login", auth.getLogin());
		model.addAttribute("modalId", modalId);

        return "organization/view";
    }

	@RequestMapping(value = "/create", method = RequestMethod.POST)
    public String actionCreate(Model model) throws IOException{

        Organization org = new Organization();
		Map<Integer, String> directionList = org.getDistrictCodeList();

        model.addAttribute("org", org);
		model.addAttribute("directionList", directionList);
		model.addAttribute("title", "Создать запись об организации");
		model.addAttribute("modalId", modalId);

        return "organization/_form";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String actionUpdate(@RequestParam(value = "id", required = true) String id, Model model) {

		Organization org = orgImpl.getOne(id).getBody();

        Map<Integer, String> directionList = org.getDistrictCodeList();

        model.addAttribute("org", org);
		model.addAttribute("directionList", directionList);
		model.addAttribute("title", "Редактировать запись об организации");
		model.addAttribute("modalId", modalId);

        return "organization/_form";
    }

	@RequestMapping(value = "/save", method = RequestMethod.POST)
    public String actionSave(@ModelAttribute("org") Organization org, Model model) {

		String method = "";
		if(!isEmpty(org.getId())){
			method = "PUT";
			responseCode = orgImpl.put(org).getStatusCodeValue();
		} else {
			method = "POST";
			responseCode = orgImpl.post(org).getStatusCodeValue();
		}


		System.out.println("responseCode " + responseCode);
		model.addAttribute("modalId", modalId);
		model = popUp.popUpModel(model, responseCode, "", "Organization", method);

        return "popUp/popUp";
    }

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String actionDelete(@RequestParam(value = "id", required = true) String id, Model model) {

		responseCode = orgImpl.delete(id).value();
		model.addAttribute("modalId", modalId);
        model = popUp.popUpModel(model, responseCode, "", "Organization", "DELETE");

        return "popUp/popUp";
    }
}
