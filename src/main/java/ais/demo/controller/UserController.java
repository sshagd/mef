/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.controller;

import ais.demo.helpers.PopUpHelper;
import ais.demo.helpers.UriHelperNotApi;
import ais.demo.helpers.UserHelper;
import ais.demo.models.user.*;
import ais.demo.service.AuthenticationFacade;
import java.io.IOException;
import java.util.*;

import ais.demo.service.UserValidator;
import org.apache.http.util.TextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import static org.springframework.util.StringUtils.isEmpty;

import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Patskevich.S
 */
@Controller
@RequestMapping("/user")
public class UserController {

	private int responseCode;
	private final String modalId = "userModal";

	@Autowired
	private AuthenticationFacade auth;
	
	@Autowired
	private UserImpl userImpl;

	@Autowired
	private UserKeysImpl userKeysImpl;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	UriHelperNotApi uriHelperN;

	@Autowired
	UserValidator validator;
	
	@Autowired
	private PopUpHelper popUp;

	@Autowired
	private UserHelper userHelper;

	@InitBinder("user")
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
    public String actionIndex(Model model){

		UserListSearch search = new UserListSearch();
		model.addAttribute("list", userImpl.getList(search).getBody());
		model.addAttribute("search", search);

        return "user/index";
    }
	
	@RequestMapping(value = "/indexRefresh", method = RequestMethod.POST)
    public String actionIndexRefresh(
				@ModelAttribute("search")UserListSearch search,
				Model model){

        model.addAttribute("list", userImpl.getList(search).getBody());
		model.addAttribute("page", search.getPage());
        model.addAttribute("startNumb", (search.getPage() - 1)  * 10);

        return "user/_index";
    }

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public String actionCreate(Model model){

		User user = new User();
		UserKeysList status = userKeysImpl.getList().getBody();
		user = userHelper.roles(user, status);
		model.addAttribute("user", user);
		model.addAttribute("status", status.getSecurityConfig());
		model.addAttribute("title", "Добавить пользователя");
		model.addAttribute("modalId", modalId);
		model.addAttribute("disabled", "false");

		return "user/_form";
	}

	@RequestMapping(value = "/view", method = RequestMethod.POST)
    public String actionView(@RequestParam(value = "id", required = true) String id, Model model){

		User user = userImpl.getOne(id).getBody();
		UserKeysList status = userKeysImpl.getList().getBody();
		user = userHelper.roles(user, status);
		model.addAttribute("status", status.getSecurityConfig());
		model.addAttribute("user", user);
		model.addAttribute("title", "Просмотр");
		model.addAttribute("modalId", modalId);

        return "user/info";
    }

	@RequestMapping(value = "/update", method = RequestMethod.POST)
    public String actionUpdate(
        @RequestParam(value = "id", required = true) String id, Model model) 
            throws IOException {

		User user = userImpl.getOne(id).getBody();
		UserKeysList status = userKeysImpl.getList().getBody();
		user = userHelper.roles(user, status);
		model.addAttribute("status", status.getSecurityConfig());
        model.addAttribute("user", user);
		model.addAttribute("title", "Редактировать пользователя");
		model.addAttribute("modalId", modalId);
		model.addAttribute("disabled", "true");

        return "user/_form";
    }

    @RequestMapping(value = "/security", method = RequestMethod.POST)
	public String actionSecurity(@ModelAttribute("user") User user, Model model){

		UserKeysList status = userKeysImpl.getList().getBody();
		UserKey key = new UserKey();
		key.setMaps(user.getMaps());
		key.setAppRoles(userHelper.setRoleList(key).getAppRoles());

		List<String> rolesList = new ArrayList<>();
		UserKeyPut put = new UserKeyPut();
		for (SecurityConfig config: status.getSecurityConfig()) {
			for (Roles roles: key.getAppRoles()) {
				put.setAppName(config.getAppName());
				put.setuId(user.getuId());
				for (AppRoles appRoles : config.getAppRoles()) {
					for (String str: roles.getRoles()) {
						if(str.equals(appRoles.getRoleName())){
							rolesList.add(str);
						}
					}
				}
			}
			put.setRoles(rolesList);
			System.out.println("RESPONSE " + userKeysImpl.put(put).getStatusCodeValue());
			rolesList.clear();
		}

		return "user/_index";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	//@ResponseBody
    public String actionSave(@ModelAttribute("user") @Validated User user, BindingResult bindingResult, Model model){

		String method = "";
		UserKeysList status = userKeysImpl.getList().getBody();
		if(bindingResult.hasErrors()){
			if(!isEmpty(user.getuId())){
				model.addAttribute("title", "Редактировать пользователя");
			} else {
				model.addAttribute("title", "Добавить пользователя");
			}
			model.addAttribute("modalId", modalId);
			model.addAttribute("user", user);

			return "user/_form";
		}

		if (!isEmpty(user.getuId())) {
			User dbUser = userImpl.getOne(user.getuId()).getBody();
			dbUser.setfN(user.getfN());
			dbUser.setsN(user.getsN());
			dbUser.setlN(user.getlN());
			dbUser.setLogin(user.getLogin());
			dbUser.setEmail(user.getEmail());
			dbUser.setSecret(BCrypt.hashpw(user.getSecret(), BCrypt.gensalt()));
			dbUser.setNotes(user.getNotes());
			dbUser.setBlocked(user.isBlocked());

			user = dbUser;
			user = userHelper.roles(user, status);
			method = "PUT";
			responseCode = userImpl.put(user).getStatusCodeValue();
		} else {
			method = "POST";
			user = userImpl.post(user);
			user = userHelper.newRoles(user, status);
		}

		System.out.println("responseCode " + responseCode);
		model.addAttribute("title", "Редактировать роли");
		model.addAttribute("status", status.getSecurityConfig());
		model.addAttribute("user", user);
		model.addAttribute("modalId", modalId);
		model = popUp.popUpModel(model, responseCode, "", "User", method);
		
        return "user/security";
    }
	
//	@RequestMapping(value = "/delete", method = RequestMethod.POST)
//    public String actionDelete(@RequestParam(value = "id", required = true) String id, Model model) {
//
//		responseCode = userImpl.delete(id).value();
//		model.addAttribute("modalId", modalId);
//        model = popUp.popUpModel(model, responseCode, "", "User", "DELETE");
//
//        return "popUp/popUp";
//    }
}
