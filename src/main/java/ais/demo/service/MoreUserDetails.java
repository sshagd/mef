/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.service;
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 *
 * @author Patskevich.S
 */
public class MoreUserDetails extends User{

	private final ais.demo.models.user.User user;

	public MoreUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, ais.demo.models.user.User user) {
		super(username, password, authorities);

		this.user = user;
	}

	public ais.demo.models.user.User getUser(){
		return user;
	}
}
