/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ais.demo.service;

import ais.demo.models.user.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 *
 * @author Patskevich.S
 */
@Component
public class AuthenticationFacade {

	public Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

	public MoreUserDetails getUserDetails(){
		return (MoreUserDetails)getAuthentication().getPrincipal();
	}

	public User getUser(){
		return (User)getUserDetails().getUser();
	}

	public String getId(){
		return getUser().getuId();
	}

//	public String getRole(){
//		return getUser().getRole();
//	}

	public String getLogin(){
		return getUser().getLogin();
	}

//	public String getOrgId(){
//		return getUser().getOrg();
//	}
}
