package ais.demo.service;

import ais.demo.helpers.UriHelperNotApi;
import ais.demo.models.user.User;
import ais.demo.models.user.UserList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.client.RestTemplate;

import static org.springframework.util.ObjectUtils.isEmpty;

@Component
public class UserValidator implements Validator {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    UriHelperNotApi uriHelperN;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        User user = (User) o;
        if(isEmpty(user.getuId())) {
            if (!isEmpty(user.getLogin())) {
                int k = 0;
                ResponseEntity<UserList> response = restTemplate.getForEntity(uriHelperN.getLogin(user.getLogin()), UserList.class);

                if (response.getBody().getTotal() > 0) {
                    for (int i = 0; i < response.getBody().getUsers().size(); i++) {
                        if (response.getBody().getUsers().get(i).getLogin().equals(user.getLogin())) {
                            k += 1;
                        }
                    }
                }
                if (k > 0) {
                    errors.rejectValue("login", "login");
                }
            } else {
                errors.rejectValue("login", "required");
            }

            if(isEmpty(user.getSecret())){
                errors.rejectValue("secret", "required");
            }

        }
    }
}
