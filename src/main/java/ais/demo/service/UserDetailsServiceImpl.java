package ais.demo.service;

import ais.demo.helpers.UriHelperNotApi;
import ais.demo.models.user.User;
import ais.demo.models.user.UserList;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	UriHelperNotApi uriHelperN;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		ResponseEntity<UserList> response = restTemplate.getForEntity(uriHelperN.getLogin(login), UserList.class);
		User user;
		String secret = restTemplate.getForEntity(
				uriHelperN.getGETUri("user", response.getBody().getUsers().get(0).getuId()), User.class)
				.getBody().getSecret();

		if(response.getBody().getTotal() == 1 && response.getBody().getUsers().get(0).getLogin().equals(login)){
			user = response.getBody().getUsers().get(0);
			user.setSecret(secret);
		} else {
			throw new UsernameNotFoundException(login);
		}
        Set<GrantedAuthority> roles = new HashSet();
//        roles.add(new SimpleGrantedAuthority(user.getRole()));

        UserDetails userDetails =
                new MoreUserDetails(user.getLogin(), user.getSecret(), roles, user);

        return userDetails;
    }

}
