$(document).on('click', '.j-create', function() {
    $.ajax({
        url: 'create',
        type:"POST",
        dataType: "html",
        success: function(data){
            $('.j-modal').html(data);
            $('#userModal').modal('show');
        }
    });
});

$(document).on('click', '.j-update', function() {
    var id = $(this).attr('data-id'); 
    $.ajax({
        url: 'update',
        data:{
            id:id
        },
        type:"POST",
        dataType: "html",
        success: function(data){
            $('.j-modal').html(data);
            $('#userModal').modal('show');
        }
    });
});

$(document).on('click', '.j-view', function() {
    var id = $(this).attr('data-id'); 
    $.ajax({
        url: 'view',
        data:{
            id:id
        },
        type:"POST",
        dataType: "html",
        success: function(data){
            $('.j-modal').html(data);
            $('#userModal').modal('show');
        }
    });
});

$(document).on('click', '.j-security', function() {
    var form = $("#user").serialize();
    $.ajax({
        url: 'security',
        data:form,
        type:"POST",
        dataType: "html",
        success: function(data){
            $('#userModal').modal('hide');
            $('#userModal').modal('show');
            var form = $("#searchForm").serialize();
            $.ajax({
                url: 'indexRefresh',
                type:"POST",
                data: form,
                dataType: "html",
                success: function(data){
                    $('.j-table').html(data);
                }
            });
        }
    });
});

$(document).on('click', '.j-save', function() {
    var form = $("#user").serialize();
    $.ajax({
        url: 'save',
        data:form,
        type:"POST",
        dataType: "html",
        success: function(data) {
            //$('.j-table').html(data);
            $('#userModal').modal('hide');
            $('body').removeClass().removeAttr('style');
            $('.modal-backdrop').remove();
            $('.j-modal').html(data);
            $('#userModal').modal('show');
        }
    });
});

$(document).on('click', '.j-delete', function(){
    var id = $(this).attr('data-id');
    $('#confirm-delete .btn-ok').attr('data-id', id);
    $('#confirm-delete').modal('show');
});

$(document).on('click', '.btn-ok', function(){
    var id = $(this).attr('data-id');
    $('#confirm-delete').modal('hide');
    $.ajax({
        url: 'delete',
        data:{
            id:id
        },
        type:"POST",
        dataType: "html",
        success: function(data){
            $('body').removeClass().removeAttr('style');
            $('.modal-backdrop').remove();
            $('.j-modal').html(data);
            $('#userModal').modal('show');
            var form = $("#searchForm").serialize();
            $.ajax({
                url: 'indexRefresh',
                type:"POST",
                data: form,
                dataType: "html",
                success: function(data){
                    $('.j-table').html(data);
                }
            });
        }
    });
});

$(document).on('click', '.j-search', function() {
    $('.j-page').val(1);
    var form = $("#searchForm").serialize();       
    $.ajax({
        url: 'indexRefresh',
        data:form,
        type:"POST",
        dataType: "html",
        success: function(data){
            $('.j-table').html(data);
            $('#pageNumb').html(1);
            $('.j-page').val(1);
        }
    });
});

$(document).on('click', '.j-next', function() {
    var page = +$(".j-page").val();
    page = page + 1;
    $('.j-page').val(page);
    var form = $("#searchForm").serialize();
    $.ajax({
        url: 'indexRefresh',
        data:form,
        type:"POST",
        dataType: "html",
        success: function(data){
            $('.j-table').html(data);
            $('#pageNumb').html(page);
        }
    });
});
$(document).on('click', '.j-prev', function() {    
    var page = +$(".j-page").val();      
    if(+page !== 1)
    {            
        page = page - 1;
        $('.j-page').val(page);
        var form = $("#searchForm").serialize();                
        $.ajax({
            url: 'indexRefresh',
            data:form,
            type:"POST",
            dataType: "html",
            success: function(data){
                $('.j-table').html(data);
                $('#pageNumb').html(page);
            }
        });
    }
});



